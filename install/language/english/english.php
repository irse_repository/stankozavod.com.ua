<?php
// Heading
$_['heading_step_1']         = 'Лицензионное соглашение';
$_['heading_step_1_small']   = 'Ознакомьтесь с условиями лицензионного соглашения   ';
$_['heading_step_2']         = 'Параметры сервера';
$_['heading_step_2_small']   = 'Пожалуйста, проверьте настройки сервера:';
$_['heading_step_3']         = 'Настройка доступа';
$_['heading_step_3_small']   = 'Укажите данные для доступа к Базе данных             ';
$_['heading_step_4']         = 'Установка успешно завершена!';
$_['heading_step_4_small']   = 'Все готово для начала продаж!';
$_['heading_maxmind']        = 'MaxMind';
$_['heading_maxmind_small']  = 'Fraud detection service.';
$_['heading_openbay']        = 'OpenBay Pro';
$_['heading_openbay_small']  = 'Multi marketplace integration';

// Text
$_['text_license']           = 'Лицензионное соглашение';
$_['text_installation']      = 'Предварительная установка';
$_['text_configuration']     = 'Настройка доступа';
$_['text_finished']          = 'Установка завершена';
$_['text_install_php']       = '1. Убедитесь, что настройки РНР соответствуют требованиям, указаным ниже.';
$_['text_install_extension'] = '2. Убедитесь, что на сервере установленны перечисленные ниже библиотеки.';
$_['text_install_db']        = '3. Please ensure you have at least one available database driver.';
$_['text_install_file']      = '3. Убедитесь, что Вы переименовали файлы, указанные ниже.';
$_['text_install_directory'] = '5. Please make sure you have set the correct permissions on the directories list below.';
$_['text_db_connection']     = '1. Укажите данные для доступа к Базе данных.';
$_['text_db_administration'] = '2. Укажите имя пользователя и пароль для Администратора.';
$_['text_congratulation']    = 'Congratulations! You have successfully installed OpenCart.';
$_['text_setting']           = 'PHP Settings';
$_['text_current']           = 'Current Settings';
$_['text_required']          = 'Required Settings';
$_['text_extension']         = 'Extension Settings';
$_['text_database']          = 'Database Driver';
$_['text_file']              = 'Files';
$_['text_directory']         = 'Directories';
$_['text_status']            = 'Status';
$_['text_version']           = 'PHP Version';
$_['text_global']            = 'Register Globals';
$_['text_magic']             = 'Magic Quotes GPC';
$_['text_file_upload']       = 'File Uploads';
$_['text_session']           = 'Session Auto Start';
$_['text_db']            	 = 'Database';
$_['text_mysqli']            = 'MySQLi';
$_['text_mysql']             = 'MySQL';
$_['text_mpdo']              = 'mPDO';
$_['text_gd']                = 'GD';
$_['text_curl']              = 'cURL';
$_['text_mcrypt']            = 'mCrypt';
$_['text_zlib']              = 'ZLIB';
$_['text_zip']               = 'ZIP';
$_['text_mbstring']          = 'mbstring';
$_['text_on']                = 'On';
$_['text_off']               = 'Off';
$_['text_writable']          = 'Writable';
$_['text_unwritable']        = 'Unwritable';
$_['text_missing']           = 'Missing';
$_['text_forget']            = 'Не забудьте удалить папку INSTALL!';
$_['text_shop']              = 'Перейти в витрину магазина';
$_['text_login']             = 'Войти в панель администратора';
$_['text_project']           = 'Страница разработчика';
$_['text_documentation']     = 'Документация';
$_['text_support']           = 'Блог NeoSeo';
$_['text_footer']            = 'Copyright © 2014 OpenCart - All rights reserved';
$_['text_loading']           = 'Loading modules...';
$_['text_store']             = 'Посетить магазин дополнений';
$_['text_services']          = 'Услуги, которые часто заказывают:';
$_['text_mail_list']         = 'Join the mailing list';
$_['text_mail_list_small']   = 'Stay informed of OpenCart updates and events.';
$_['text_openbay']   		 = 'OpenBay Pro gives merchants the ability to link their store with 3rd party markets like eBay and Amazon. Import orders, list items and handle shipping information direct from OpenCart...';
$_['text_maxmind']   		 = 'MaxMind provides merchants the ability to identify risky transactions quickly, reducing the risk of fraud and minimises the time spent reviewing orders by giving a risk score for each one...';
$_['text_more_info']   		 = 'More information';
$_['text_facebook']   		 = 'Like us on Facebook';
$_['text_facebook_info']     = 'Tell us how much you like OpenCart!';
$_['text_facebook_link']     = 'Visit our Facebook page';
$_['text_forum']    	 	 = 'Community forums';
$_['text_forum_info']    	 = 'Get help from other OpenCart users';
$_['text_forum_link']    	 = 'Visit our forums';
$_['text_commercial']    	 = 'Commercial support';
$_['text_commercial_info']   = 'Development services from OpenCart partners';
$_['text_commercial_link']   = 'Visit our partner page';
$_['text_view']   		     = 'View details';
$_['text_download']   		 = 'Download';
$_['text_downloads']   		 = 'Downloads';
$_['text_price']   		 	 = 'Price';
$_['text_view']   		     = 'View details';
$_['text_maxmind_success']   = 'MaxMind fraud service has been installed';
$_['text_maxmind_top']   	 = 'If you don\'t have a license key you can';
$_['text_maxmind_link']   	 = 'sign up here';
$_['text_ebay_about']	 	 = 'eBay is a multi-billion dollar market place that allows business or private sellers to auction and retail goods online. Available to sellers worldwide.';
$_['text_amazon_about']	 	 = 'Amazon Marketplace a fixed-price online marketplace allowing sellers to offer new and used items alongside Amazon\'s regular retail service.';

// Entry
$_['entry_db_driver']        = 'Драйвер БД';
$_['entry_db_hostname']      = 'Сервер';
$_['entry_db_username']      = 'Логин';
$_['entry_db_password']      = 'Пароль';
$_['entry_db_database']      = 'База данных';
$_['entry_db_port']          = 'Порт';
$_['entry_db_prefix']        = 'Префикс';
$_['entry_username']         = 'Логин';
$_['entry_password']         = 'Пароль';
$_['entry_email']            = 'E-Mail';
$_['entry_key']              = 'License Key';
$_['entry_score']      		 = 'Risk score';
$_['entry_order_status']     = 'Fraud Order Status';

// Help
$_['help_score']             = 'The higher the score the more likely the order is fraudulent. Set a score between 0 - 100.';
$_['help_order_status']      = 'Orders over your set score will be assigned this order status and will not be allowed to reach the complete status automatically.';

// Error
$_['error_key'] 			 = 'Maxmind licence key required';
$_['error_score'] 			 = 'A score between 0 and 100 is accepted';
$_['error_db_hostname'] 	 = 'Hostname required!';
$_['error_db_username'] 	 = 'Username required!';
$_['error_db_database']		 = 'Database Name required!';
$_['error_db_port']		     = 'Database Port required!';
$_['error_db_prefix'] 		 = 'DB Prefix can only contain lowercase characters in the a-z range, 0-9 and underscores';
$_['error_db_connect'] 		 = 'Error: Could not connect to the database please make sure the database server, username and password is correct!';
$_['error_username'] 		 = 'Username required!';
$_['error_password'] 		 = 'Password required!';
$_['error_email'] 			 = 'Invalid E-Mail!';
$_['error_config'] 			 = 'Error: Could not write to config.php please check you have set the correct permissions on: ';

// Buttons
$_['button_continue']        = 'Продолжить';
$_['button_back']            = 'Назад';
$_['button_join']            = 'Join here';
$_['button_setup']           = 'Set-up now';
$_['button_register']        = 'Register';
$_['button_register_eu']     = 'Register Europe';
$_['button_register_us']     = 'Register USA';

$_['text_terms'] = '<h3><b>Политика конфедициальности</b></h3><br>
        <p>Сайт  разрешает вам просматривать и загружать материалы этого сайта (далее «Сайт») только для 
личного некоммерческого использования, при условии сохранения вами всей информации об 
авторском праве и других сведений о праве собственности, содержащихся в исходных материалах 
и любых их копиях. Запрещается изменять материалы этого Сайта, а также распространять или 
демонстрировать их в любом виде или использовать их любым другим образом для общественных 
или коммерческих целей. Любое использование этих материалов на других сайтах или в 
компьютерных сетях запрещается.</p>

 <p>Сайт  разрешает вам просматривать и загружать материалы этого сайта (далее «Сайт») только для 
личного некоммерческого использования, при условии сохранения вами всей информации об 
авторском праве и других сведений о праве собственности, содержащихся в исходных материалах 
и любых их копиях. Запрещается изменять материалы этого Сайта, а также распространять или 
демонстрировать их в любом виде или использовать их любым другим образом для общественных 
или коммерческих целей. Любое использование этих материалов на других сайтах или в 
компьютерных сетях запрещается.</p>
';


$_['text_individual_design'] = '
<p><b>Индивидуальный дизайн</b> или стилизация Интернет-магазина на OpenCart</p>
<a href="https://neoseo.com.ua/dizayn-internet-magazina">https://neoseo.com.ua/dizayn-internet-magazina</a>';

$_['text_seo_structure'] = '<p>Создание <b>"SEO-структуры"</b> для <br>Интернет-магазина</p>
<a href="https://neoseo.com.ua/seo-structure-internet-shop">https://neoseo.com.ua/seo-structure-internet-shop</a>';

$_['text_optimization'] = '<p><b>Услуга по ускорению и оптимизации</b> скорости загрузки страниц <br>Интернет-магазина по тесту Google PageSpeedInsights</p>
<a href="https://neoseo.com.ua/google-page-speed-opencart">https://neoseo.com.ua/google-page-speed-opencart</a>';

$_['text_inside_optimization'] = ' <p><b>Внутренняя техническая</b><br>интернет-магазина (для SEO)</p>
<a href="https://neoseo.com.ua/internal-seo-optimization-opencart">https://neoseo.com.ua/internal-seo-optimization-opencart</a>';

$_['text_facebook'] = '
       <div class="share-title">
          <img class="img-responsive" src="view/image/facebook.png" alt="NeoSeo" title="NeoSeo" />
          <span>Поставь лайк</span>
        </div>
        <ul class="share-list-link">
          <li class="first"><i class="fa fa-caret-right"></i><a href="">Поделиться с друзьями</a></li>
          <li><i class="fa fa-caret-right"></i>Посетить страницу <a href="">facebook</a></li>
        </ul>';


$_['text_google'] = '
<div class="share-title">
          <img class="img-responsive" src="view/image/google+.png" alt="NeoSeo" title="NeoSeo" />
          <span>Поставь лайк</span>
        </div>
        <ul class="share-list-link">
          <li class="first"><i class="fa fa-caret-right"></i><a href="">Поделиться с друзьями</a></li>
          <li><i class="fa fa-caret-right"></i>Посетить страницу <a href="">google+</a></li>
        </ul>
';

$_['text_blog'] = '
<div class="share-title">
          <img class="img-responsive" src="view/image/blog.png" alt="NeoSeo" title="NeoSeo" />
          <span>Блог NeoSeo</span>
        </div>
        <ul class="share-list-link">
          <li class="first"><i class="fa fa-caret-right"></i><a href="">Получить помощь NeoSeo</a></li>
          <li><i class="fa fa-caret-right"></i>Посетить <a href="">Блог NeoSeo</a></li>
        </ul>
        ';

$_['text_subscribe'] = '
<div class="text">
            <p>Подписаться на рассылку</p>
            <div class="form-group">
              <input type="checkbox">
              <label>Я хочу получать полезные уведомления на почту про обновления, модули, акции</div>
            </div>';