<?php echo $header; ?>
<div class="container">

  <header>
    <div class="col-sm-9">

      <div class="licence-argeement">
        <h1 class="license-step"><?php echo $text_upgrade; ?></h1>
      </div>

      <div class="form-license-agreement">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <fieldset>
            <?php echo $text_instruction; ?>
          </fieldset>
          <div class="buttons">
            <input type="submit" class="__button-next" value="<?php echo $button_continue; ?>">
          </div>
        </form>
      </div>


    </div>
    <div class="col-sm-3">
      <div class="logo" style="display: inline-flex">
        <a href="https://neoseo.com.ua/yunistor-firmennaya-tema-oformleniya" target="_blank">
          <img class="img-responsive" src="view/image/neoseo-logo.png" alt="NeoSeo" title="NeoSeo" />
        </a>
      </div>
      <ul class="nav-group">
        <li class="nav-group-item"><i class="fa fa-chevron-right"></i><span class="active"><?php echo $text_upgrade; ?></span></li>
        <li class="nav-group-item"><?php echo $text_finished; ?></li>
      </ul>
    </div>
  </header>

</div>
<br/>
<br/>
<?php echo $footer; ?>