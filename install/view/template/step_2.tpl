<?php echo $header; ?>
<div class="container">
  <header>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row">
      <div class="col-sm-9">
        <div class="licence-argeement">
          <h1 class="license-step">2<small>/4</small></h1>
          <h3 class="licence-title"><?php echo $heading_step_2; ?><br>
            <small><?php echo $heading_step_2_small; ?></small></h3>
        </div>

        <div class="form-install-extensions">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <p><?php echo $text_install_php; ?></p>
            <fieldset>
              <table class="table">
                <thead>
                <tr>
                  <td style="padding-left: 25px;" width="25%"><b><?php echo $text_setting; ?></b></td>
                  <td width="25%" class="text-center"><b><?php echo $text_current; ?></b></td>
                  <td width="25%" class="text-center"><b><?php echo $text_required; ?></b></td>
                  <td width="25%" class="text-center"><b><?php echo $text_status; ?></b></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td style="padding-left: 25px;"><?php echo $text_version; ?></td>
                  <td class="text-center"><?php echo $php_version; ?></td>
                  <td class="text-center">5.3+</td>
                  <td class="text-center"><?php if ($php_version >= '5.3') { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px;"><?php echo $text_global; ?></td>
                  <td class="text-center"><?php if ($register_globals) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_off; ?></td>
                  <td class="text-center"><?php if (!$register_globals) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px;"><?php echo $text_magic; ?></td>
                  <td class="text-center"><?php if ($magic_quotes_gpc) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_off; ?></td>
                  <td class="text-center"><?php if (!$magic_quotes_gpc) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px;"><?php echo $text_file_upload; ?></td>
                  <td class="text-center"><?php if ($file_uploads) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($file_uploads) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px;"><?php echo $text_session; ?></td>
                  <td class="text-center"><?php if ($session_auto_start) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_off; ?></td>
                  <td class="text-center"><?php if (!$session_auto_start) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                </tbody>
              </table>
            </fieldset>
            <p><?php echo $text_install_extension; ?></p>
            <fieldset>
              <table class="table">
                <thead>
                <tr>
                  <td width="25%" class="text-center"><b><?php echo $text_extension; ?></b></td>
                  <td width="25%" class="text-center"><b><?php echo $text_current; ?></b></td>
                  <td width="25%" class="text-center"><b><?php echo $text_required; ?></b></td>
                  <td width="25%" class="text-center"><b><?php echo $text_status; ?></b></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td style="padding-left: 25px"><?php echo $text_db; ?></td>
                  <td class="text-center"><?php if ($db) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($db) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px"><?php echo $text_gd; ?></td>
                  <td class="text-center"><?php if ($gd) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($gd) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px"><?php echo $text_curl; ?></td>
                  <td class="text-center"><?php if ($curl) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($curl) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px"><?php echo $text_mcrypt; ?></td>
                  <td class="text-center"><?php if ($mcrypt_encrypt) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($mcrypt_encrypt) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px"><?php echo $text_zlib; ?></td>
                  <td class="text-center"><?php if ($zlib) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($zlib) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px"><?php echo $text_zip; ?></td>
                  <td class="text-center"><?php if ($zip) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center"><?php if ($zip) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <?php if (!$iconv) { ?>
                <tr>
                  <td style="padding-left: 25px;"><?php echo $text_mbstring; ?></td>
                  <td class="text-center">
                    <?php if ($mbstring) { ?>
                    <?php echo $text_on; ?>
                    <?php } else { ?>
                    <?php echo $text_off; ?>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $text_on; ?></td>
                  <td class="text-center">
                    <?php if ($mbstring) { ?>
                    <span class="text-primary"><i class="fa fa-check-circle"></i></span>
                    <?php } else { ?>
                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                    <?php } ?>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </fieldset>
            <p><?php echo $text_install_file; ?></p>
            <fieldset>
              <table class="table">
                <thead>
                <tr>
                  <td style="width: 50%;padding-left: 25px"><b><?php echo $text_file; ?></b></td>
                  <td style="width: 50%;"><b><?php echo $text_status; ?></b></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td style="padding-left: 25px"><?php echo $config_catalog; ?></td>
                  <td><?php if (!file_exists($config_catalog)) { ?>
                    <span class="text-danger"><?php echo $text_missing; ?></span>
                    <?php } elseif (!is_writable($config_catalog)) { ?>
                    <span class="text-danger"><?php echo $text_unwritable; ?></span>
                    <?php } else { ?>
                    <span class="text-done"><?php echo $text_writable; ?></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td style="padding-left: 25px"><?php echo $config_admin; ?></td>
                  <td><?php if (!file_exists($config_admin)) { ?>
                    <span class="text-danger"><?php echo $text_missing; ?></span>
                    <?php } elseif (!is_writable($config_admin)) { ?>
                    <span class="text-danger"><?php echo $text_unwritable; ?></span>
                    <?php } else { ?>
                    <span class="text-primary"><?php echo $text_writable; ?></span>
                    <?php } ?>
                  </td>
                </tr>
                </tbody>
              </table>
            </fieldset>

            <div class="buttons space-between">
                <a href="<?php echo $back; ?>" class="__button-prev"><?php echo $button_back; ?></a>
              <div>
                  <input type="submit" class="__button-next" value="<?php echo $button_continue; ?>">
              </div>
            </div>
          </form>
        </div>


      </div>
      <div class="col-sm-3">
        <div class="logo sm">
          <a href="https://neoseo.com.ua/yunistor-firmennaya-tema-oformleniya" target="_blank">
            <img class="img-responsive" src="view/image/neoseo-logo.png" alt="NeoSeo" title="NeoSeo" />
          </a>
        </div>
        <ul class="nav-group">
          <li class="nav-group-item"><span><?php echo $text_license; ?></span></li>
          <li class="nav-group-item"><i class="fa fa-chevron-right"></i> <span  class="active"><?php echo $text_installation; ?></span></li>
          <li class="nav-group-item"><span><?php echo $text_configuration; ?></span></li>
          <li class="nav-group-item"><span><?php echo $text_finished; ?></span></li>
        </ul>
      </div>
    </div>
  </header>

</div>
<?php echo $footer; ?>