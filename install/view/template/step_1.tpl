<?php echo $header; ?>
<div class="container">
  <header>
    <div class="col-sm-9">

      <div class="licence-argeement">
        <h1 class="license-step">1<small>/4</small></h1>
        <h3 class="licence-title"><?php echo $heading_step_1; ?><br>
          <small><?php echo $heading_step_1_small; ?></small></h3>
      </div>

      <div class="form-license-agreement">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="terms"><?php echo $text_terms; ?></div>
          <div class="buttons">
            <input type="submit" class="__button-next" value="<?php echo $button_continue; ?>">
          </div>
        </form>
      </div>


    </div>
    <div class="col-sm-3">
      <div class="logo">
        <a href="https://neoseo.com.ua/yunistor-firmennaya-tema-oformleniya" target="_blank">
          <img class="img-responsive" src="view/image/neoseo-logo.png" alt="NeoSeo" title="NeoSeo" />
        </a>
      </div>
      <ul class="nav-group">
        <li class="nav-group-item"><i class="fa fa-chevron-right"></i><span class="active"><?php echo $text_license; ?></span></li>
        <li class="nav-group-item"><span><?php echo $text_installation; ?></span></li>
        <li class="nav-group-item"><span><?php echo $text_configuration; ?></span></li>
        <li class="nav-group-item"><span><?php echo $text_finished; ?></span></li>
      </ul>
    </div>
  </header>

</div>
<?php echo $footer; ?>