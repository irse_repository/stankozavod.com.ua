<?php echo $header; ?>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><?php echo $success; ?></div>
  <?php } ?>
  <header class="clear">
    <div class="row">
      <div class="col-sm-9">
        <div class="licence-argeement">
          <h1 class="license-step">4<small>/4</small></h1>
          <h3 class="licence-title"><?php echo $heading_step_4; ?><br>
            <small><?php echo $heading_step_4_small; ?></small></h3>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="logo">
          <a href="https://neoseo.com.ua/yunistor-firmennaya-tema-oformleniya" target="_blank">
            <img class="img-responsive" src="view/image/neoseo-logo.png" alt="NeoSeo" title="NeoSeo" />
          </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="text-forget">
          <span><?php echo $text_forget; ?></span>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="online-shop">
  <div class="container">
    <div class="row">
      <div class="go-to-shop col-sm-6">
        <a href="../"><span><?php echo $text_shop; ?></span></a>
      </div>
      <div class="go-to-admin col-sm-6">
        <a href="../admin/"><span><?php echo $text_login; ?></span></a>
      </div>
    </div>
    <div class="row" id="modules" style="display:none;"></div>
    <div class="row">
      <div class="col-sm-12">
        <div class="extensions-box">
          <a class="go-to-extensions" href="https://neoseo.com.ua/module-prices-in-rubles " target="_blank"><?php echo $text_store; ?></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="services-title text-center">
          <?php echo $text_services; ?>
        </div>
        <div class="services-box">
          <div class="col-sm-6 right bottom">
            <div class="services-item">
              <img class="img-responsive" src="view/image/individual-design.png" alt="NeoSeo" title="NeoSeo" />
              <div class="content">
                <?php echo $text_individual_design; ?>
              </div>
            </div>
          </div>
          <div class="col-sm-6 bottom">
            <div class="services-item ">
              <img class="img-responsive" src="view/image/seo-structure.png" alt="NeoSeo" title="NeoSeo" />
              <div class="content">
                <?php echo $text_seo_structure; ?>
              </div>
            </div>
          </div>
          <div class="col-sm-6 right">
            <div class="services-item">
              <img class="img-responsive" src="view/image/optimization.png" alt="NeoSeo" title="NeoSeo" />
              <div class="content">
                <?php echo $text_optimization; ?>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="services-item">
              <img class="img-responsive" src="view/image/inside-optimization.png" alt="NeoSeo" title="NeoSeo" />
              <div class="content">
                <?php echo $text_inside_optimization; ?>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="share">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <?php echo $text_facebook; ?>

      </div>
      <div class="col-sm-4">
        <?php echo $text_google; ?>

      </div>
      <div class="col-sm-4">
        <?php echo $text_blog; ?>

      </div>
    </div>
  </div>
</section>
<div class="footer">
  <?php echo $footer; ?>
</div>

<script type="text/javascript"><!--
function searchExtensions() {
  var html = '';

  $.ajax({
    url: 'index.php?route=step_4/extensions',
    type: 'post',
    dataType: 'json',
    beforeSend: function() {
      $('#modules-loading').show();
      $('#modules').empty().hide();
    },
    success: function(json) {
      $.each (json.extensions, function(key, val) {
        html = '<div class="col-sm-6 module">';
          html += '<a class="thumbnail pull-left" href="'+val.href+'"><img src="'+val.image+'" alt="'+val.name+'"></a>';
          html += '<h5>'+val.name+'</h5>';
          html += '<p>'+val.price+' <a target="_BLANK" href="'+val.href+'"><?php echo $text_view; ?></a></p>';
          html += '<div class="clearfix"></div>';
        html += '</div>';

        $('#modules').append(html);
      });

      $('#modules').fadeIn();
      $('#modules-loading').hide();
    },
    failure: function() {
      $('#modules-loading').hide();
    },
    error: function() {
      $('#modules-loading').hide();
    }
  });
}
function searchLanguages() {
  var html = '';

  $.ajax({
    url: 'index.php?route=step_4/language',
    type: 'post',
    data: {'language' : '<?php echo $language; ?>' },
    dataType: 'json',
    beforeSend: function() {
      $('#module-language').empty().hide();
    },
    success: function(json) {
      if (json.extension != '') {
        html = '<div class="row">';
          html += '<div class="col-sm-12">';
            html += '<img class="img-rounded" src="'+json.extension.image+'">';
            html += '<h3>'+json.extension.name+'<br><small><?php echo $text_downloads; ?>: '+json.extension.downloaded+', <?php echo $text_price; ?>: '+json.extension.price+'</small></h3>';
            html += '<a class="btn btn-primary" href="'+json.extension.href+'" target="_BLANK"><?php echo $text_download; ?></a>';
          html += '</div>';
        html += '</div>';

        $('#module-language').html(html).fadeIn();
      }
    },
    failure: function() { },
    error: function() { }
  });
}
$( document ).ready(function() {
  searchExtensions();
  searchLanguages();
});
//--></script>