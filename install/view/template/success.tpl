<?php echo $header; ?>
<div class="container">
  <header>
    <div class="row">
      <div class="col-sm-12">
        <a style="display: inline-flex" href="https://neoseo.com.ua/yunistor-firmennaya-tema-oformleniya" target="_blank">
          <img class="img-responsive" src="view/image/neoseo-logo.png" alt="NeoSeo" title="NeoSeo" />
        </a>
      </div>
    </div>
  </header>
  <h1>Обновление - завершено!</h1>
  <div class="row">
    <div class="col-sm-9">
      <div class="alert alert-danger">Не забудьте удалить каталог установки!</div>
      <p>Поздравляем! Вы успешно обновили OpenCart.</p>
      <div class="row">
        <div class="col-lg-6"><a href="../"><img src="view/image/screenshot_1.png" alt="" class="img-thumbnail" /></a><br />
          <p class="text-center"><a href="../">Перейти в интернет-магазин</a></p>
        </div>
        <div class="col-lg-6"><a href="../admin/"><img src="view/image/screenshot_2.png" alt="" class="img-thumbnail" /></a><br />
          <p class="text-center"><a href="../admin/">Войти в админку</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <ul class="nav-group">
        <li class="nav-group-item">Обновлено</li>
        <li class="nav-group-item"><i class="fa fa-chevron-right"></i><span class="active">Установка завершена</span></li>
      </ul>
    </div>
  </div>
</div>
<br/>
<br/>
<?php echo $footer; ?>