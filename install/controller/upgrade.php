<?php
class ControllerUpgrade extends Controller {
    private $error = array();

    public function index() {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('upgrade');

            $this->model_upgrade->mysql();

            $this->response->redirect($this->url->link('upgrade/success'));
        }

        $data = array();

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['action'] = $this->url->link('upgrade');

        $data['text_license'] = $this->language->get('text_license');
        $data['text_installation'] = $this->language->get('text_installation');
        $data['text_configuration'] = $this->language->get('text_configuration');
        $data['text_upgrade'] = $this->language->get('text_upgrade');
        $data['text_finished'] = $this->language->get('text_finished');
        $data['text_terms'] = $this->language->get('text_terms');
        $data['text_instruction'] = $this->language->get('text_instruction');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        $this->response->setOutput($this->load->view('upgrade.tpl', $data));
    }

    private function validate() {
        if (DB_DRIVER == 'mysql') {
            if (!$connection = @mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD)) {
                $this->error['warning'] = 'Error: Could not connect to the database please make sure the database server, username and password is correct in the config.php file!';
            } else {
                if (!mysql_select_db(DB_DATABASE, $connection)) {
                    $this->error['warning'] = 'Error: Database "' . DB_DATABASE . '" does not exist!';
                }

                mysql_close($connection);
            }
        }

        return !$this->error;
    }

    public function success() {
        $data = array();

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        $this->response->setOutput($this->load->view('success.tpl', $data));
    }
}